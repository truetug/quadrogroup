"""application URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken.views import obtain_auth_token

from core.views import (
    login_api_view, logout_api_view, 
    StatisticViewSet, CategoryViewSet, ItemViewSet
)


router = DefaultRouter()
router.register(r'stat', StatisticViewSet)
router.register(r'category', CategoryViewSet)
router.register(r'item', ItemViewSet)


urlpatterns = [
    url(
        r'^api-auth/',
        include('rest_framework.urls', namespace='rest_framework')
    ),
    url(r'^api-token-auth/', obtain_auth_token),
    url(r'^app/api/login/', login_api_view, name='api-login'),
    url(r'^app/api/logout/', logout_api_view, name='api-logout'),
    url(r'^app/api/', include(router.urls, namespace='api')),
    url(r'^admin/', admin.site.urls),
]

from django.conf.urls.static import static
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()
