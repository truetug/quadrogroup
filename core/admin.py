from django.contrib import admin

from .models import Statistic, Category, Item


@admin.register(Statistic)
class StatisticAdmin(admin.ModelAdmin):
	list_display = (
		'city', 'country', 'population'
	)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
	pass


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
	list_display = (
		'name', 'admin_categories', 'value_int', 'value_float'
	)

	def get_queryset(self, *args, **kwargs):
		qs = super(ItemAdmin, self).get_queryset(*args, **kwargs)
		qs = qs.prefetch_related('categories')

		return qs

	def admin_categories(self, obj):
		return ', '.join([x.name for x in obj.categories.all()])

