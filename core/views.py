from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.viewsets import GenericViewSet, ModelViewSet
from rest_framework.mixins import RetrieveModelMixin, ListModelMixin
from rest_framework.pagination import PageNumberPagination
from rest_framework.authentication import BaseAuthentication
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import exceptions

from .models import Statistic, Category, Item
from .serializers import (
    StatisticSerializer, 
    CategorySerializer, 
    ItemSerializer,
    UserSerializer,
)


@api_view(['GET', 'POST'])
def login_api_view(request):
    """
    browserless login
    """
    user = request.user
    if not user.is_authenticated():
        logout(request)
        user = authenticate(
            username=request.POST.get('username'), 
            password=request.POST.get('password'),
        )

    if user is not None and user.is_active:
        login(request, user)
        data = {
            'status': 'success',
            'message': 'You are logged in',
            'body': UserSerializer(user).data,
        }
        status = 200
    else:
        data = {
            'status': 'error',
            'message': 'Something went wrong',
        }
        status = 403

    return Response(data, status=status)


@api_view(['GET'])
def logout_api_view(request):
    """
    browserless logout
    """
    logout(request)
    data = {
        'status': 'success',
        'message': 'You are logged out',
    }
    return Response(data)


class BaseResultsSetPagination(PageNumberPagination):
    page_size = 2
    page_size_query_param = 'page_size'
    max_page_size = 10


class ApiBaseMixin(object):
    # see settings.py
    # authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated, )
    pagination_class = BaseResultsSetPagination


class StatisticViewSet(ApiBaseMixin, RetrieveModelMixin, ListModelMixin, GenericViewSet):
    serializer_class = StatisticSerializer
    queryset = Statistic.objects.all()


class CategoryViewSet(ApiBaseMixin, ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class ItemViewSet(ApiBaseMixin, ModelViewSet):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()
